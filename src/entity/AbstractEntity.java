package entity;

import java.io.Serializable;

public interface AbstractEntity extends Serializable {

	public Number getId();
	
}
