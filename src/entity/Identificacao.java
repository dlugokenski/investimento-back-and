package entity;

public class Identificacao {

	private String email;
	
	public Identificacao(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
