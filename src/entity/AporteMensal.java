package entity;

import java.util.List;

public class AporteMensal {

	private int mesCopetencia;
	private int anoCopetencia;
	private float valorTotal;
	private List<Trade> listaDeTrade;
	
	public AporteMensal(){
		
	}
	
	public AporteMensal(int mesCopetencia, int anoCopetencia, List<Trade> listaDeTrade){
		this.anoCopetencia = anoCopetencia;
		this.mesCopetencia = mesCopetencia;
		this.listaDeTrade = listaDeTrade; 
	}

	public int getMesCopetencia() {
		return mesCopetencia;
	}

	public void setMesCopetencia(int mesCopetencia) {
		this.mesCopetencia = mesCopetencia;
	}

	public int getAnoCopetencia() {
		return anoCopetencia;
	}

	public void setAnoCopetencia(int anoCopetencia) {
		this.anoCopetencia = anoCopetencia;
	}

	public float getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(float valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<Trade> getListaDeTrade() {
		return listaDeTrade;
	}

	public void setListaDeTrade(List<Trade> listaDeTrade) {
		this.listaDeTrade = listaDeTrade;
	}
	
	
}
