package entity;

import java.time.LocalTime;

public class Ativo implements AbstractEntity{

	private int id;
	private String codigo;
	private float preco;
	private LocalTime ultimaDataAtualizacao;
	
	public Ativo(){
		
	}
	
	public Ativo(String codigo, float preco){
		this.codigo = codigo;
		this.preco = preco;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}

	public LocalTime getUltimaDataAtualizacao() {
		return ultimaDataAtualizacao;
	}

	public void setUltimaDataAtualizacao(LocalTime ultimaDataAtualizacao) {
		this.ultimaDataAtualizacao = ultimaDataAtualizacao;
	}

	public Number getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
