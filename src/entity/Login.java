package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="autenticacao")
public class Login implements AbstractEntity{
	
	@Id
    @GeneratedValue
    @Column(name="id_usuario")
	private long id;
	
	@Column(name="senha")
	private String password;
	
	@Column(name="cadastro_validado")
	private String cadastroValidado;
	
	@Column(name="conta_ativa")
	private boolean contaAtiva;
	
	@Column(name="senha_propria")
	private boolean senhaPropria;
	
	public Login() {
		
	}
	
	public Login(long id, String password, String cadastroValidado, boolean contaAtiva, boolean senhaPropria) {
		this.id = id;
		this.password = password;
		this.cadastroValidado = cadastroValidado;
		this.contaAtiva = contaAtiva;		
		this.senhaPropria = senhaPropria;

	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getCadastroValidado() {
		return cadastroValidado;
	}
	
	public void setCadastroValidado(String cadastroValidado) {
		this.cadastroValidado = cadastroValidado;
	}
	
	public boolean isContaAtiva() {
		return contaAtiva;
	}
	
	public void setContaAtiva(boolean contaAtiva) {
		this.contaAtiva = contaAtiva;
	}
	
	public boolean isSenhaPropria() {
		return senhaPropria;
	}
	
	public void setSenhaPropria(boolean senhaPropria) {
		this.senhaPropria = senhaPropria;
	}
	
	public Number getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
