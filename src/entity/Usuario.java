package entity;

public class Usuario extends Pessoa{

	private Identificacao Identificacao;
	private Endereco endereco;
	
	
	public Usuario(){

	}
	
	public Usuario(Identificacao Identificacao){
		this.Identificacao = Identificacao;
	}
	
	public Identificacao getIdentificacao() {
		return Identificacao;
	}
	
	public void setIdentificacao(Identificacao identificacao) {
		Identificacao = identificacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
}
