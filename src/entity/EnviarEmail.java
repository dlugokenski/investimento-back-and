package entity;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class EnviarEmail implements Runnable {
	
	StringBuilder corpoDoEmail;
	Usuario usuario;
	String email;
	String senha;
	String assunto;
	
	public EnviarEmail(Usuario usuario, String email, String senha, String assunto, StringBuilder corpoDoEmail){
		this.email = email;
		this.senha = senha;
		this.usuario = usuario;
		this.assunto = assunto;
		this.corpoDoEmail = corpoDoEmail;
	}
	public void run(){
						
		HtmlEmail emailHost = new HtmlEmail();
		emailHost.setDebug(true); 
		emailHost.setCharset("UTF-8");
		emailHost.setHostName("smtp.gmail.com");  
		emailHost.setAuthentication(email,senha);  
		emailHost.setTLS(true);;
		emailHost.setSmtpPort(587);
		try {
		emailHost.setFrom(email);
		emailHost.setSubject(assunto);
		emailHost.setHtmlMsg(corpoDoEmail.toString());
		emailHost.addTo(usuario.getIdentificacao().getEmail());
		emailHost.send();
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	   	
}
