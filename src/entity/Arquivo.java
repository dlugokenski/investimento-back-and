package entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.FormParam;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

@Entity 
@Table(name="usuario")
public class Arquivo implements Serializable{

	@Id
    @GeneratedValue
    @Column(name="id_usuario")
	private long idUsuario;
	 
	@Column(name="avatar")
	private byte[] file;
	
	public Arquivo(){
		
	}
	
	public Arquivo(byte[] file, long idUsuario){
		this.file = file;
		this.idUsuario = idUsuario;
	}
	
	public byte[] getFile() {
		return file;
	}
	
	@FormParam("uploadedFile")
	@PartType("application/octet-stream")
	public void setFile(byte[] file) {
		this.file = file;
	}
	
	public long getIdUsuario() {
		return idUsuario;
	}
	
	@FormParam("idUsuario")
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
}
