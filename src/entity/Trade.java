package entity;

import java.time.LocalDate;

public class Trade extends Ativo {
	
	private LocalDate data;
	private String compraVenda;
	private float quantidade;
	private int numeroDoTrade;
	private float valorTotalOperacao;
	private float resultadoFinanceiro;
	private String tipo;

	public Trade(LocalDate data, String compraVenda, String ativo, float quantidade, float preco, int numeroDoTrade, float valorTotalOperacao) {
		this.data = data;
		this.compraVenda = compraVenda;
		super.setCodigo(ativo);
		this.quantidade = quantidade;
		super.setPreco(preco);;
		this.numeroDoTrade = numeroDoTrade;
		this.valorTotalOperacao = valorTotalOperacao;
	}
	
	public Trade(LocalDate data, String compraVenda, String ativo, float quantidade, float preco, int numeroDoTrade, float valorTotalOperacao, String tipo) {
		this.data = data;
		this.compraVenda = compraVenda;
		super.setCodigo(ativo);
		this.quantidade = quantidade;
		super.setPreco(preco);;
		this.numeroDoTrade = numeroDoTrade;
		this.valorTotalOperacao = valorTotalOperacao;
		this.tipo = tipo;
	}

	public Trade (String ativo,float resultadoFinanceiro){
		super.setCodigo(ativo);
		this.resultadoFinanceiro = resultadoFinanceiro;
	}
	 
	public double getResultadoFinanceiro() {
		return resultadoFinanceiro;
	}

	public void setResultadoFinanceiro(float resultadoFinanceiro) {
		this.resultadoFinanceiro = resultadoFinanceiro;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public String getCompraVenda() {
		return compraVenda;
	}

	public void setCompraVenda(String compraVenda) {
		this.compraVenda = compraVenda;
	}

	public float getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}

	public int getNumeroDoTrade() {
		return numeroDoTrade;
	}

	public void setNumeroDoTrade(int numeroDoTrade) {
		this.numeroDoTrade = numeroDoTrade;
	}

	public float getValorTotalOperacao() {
		return valorTotalOperacao;
	}

	public void setValorTotalOperacao(float valorTotalOperacao) {
		this.valorTotalOperacao = valorTotalOperacao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
