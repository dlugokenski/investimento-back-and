package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.persistence.Query;

import entity.Trade;
import util.Converter;


@Dependent
public class CarteiraDAO extends AbstractPersistence<Trade, Long> implements DefaultService<Trade>{
	
	public CarteiraDAO() {
		super(Trade.class);
	}

	public boolean verificarSeTituloPublicoExiste(String titulo) {
		String queryString = "SELECT count(*) > 0 FROM titulo_publico WHERE titulo = :titulo ";
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("titulo", titulo);
		return (boolean) query.getSingleResult();
	}
	
	public boolean verificarSeAcaoExiste(String ativo) {
		String queryString = "SELECT count(*) > 0 FROM cotacao WHERE ativo = :ativo ";
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("ativo", ativo);
		return (boolean) query.getSingleResult();
	}
	
	public float pegarCotacaoAtualAcao(String ativo) {
		String queryString = "SELECT preco FROM cotacao WHERE ativo = :ativo ";
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("ativo", ativo);
		return (float) query.getSingleResult();		
	}
	
	public float pegarCotacaoAtualTesouro(String titulo) {
		String queryString = "SELECT valor_unidade FROM titulo_publico WHERE titulo = :titulo ";
		Query query = getEntityManager().createNativeQuery(queryString);
		query.setParameter("titulo", titulo);
		return (float) query.getSingleResult();
	}
	
	public String pergarUltimaDataAtualizacao() {
		String queryString = "SELECT MAX(ultima_atualizacao) FROM cotacao";
		return (String) getEntityManager().createNativeQuery(queryString).getResultList().get(0);
	}
	
	public void cadastrarCadastroCarteira(Trade trade, long idUsuario, int numeroCarteira) throws SQLException{
//		EntityManagerFactory factory = Persistence.createEntityManagerFactory("investor");
//		EntityManager manager = factory.createEntityManager();
//		manager.getTransaction().begin();
//		manager.merge(file);
//		manager.getTransaction().commit();
//		manager.close();
		
//		Connection conexao = FabricaDeConexao.getConnection();
//		String querySql;
//
//		querySql = "INSERT INTO public.carteira(id_usuario,numero_carteira,ativo,quantidade,preco,data,tipo_orden)	values(?,?,?,?,?,?,?) ";
//				  
//		PreparedStatement stmt = conexao.prepareStatement(querySql);
//		
//		stmt.setLong(1, idUsuario);
//		stmt.setInt(2, numeroCarteira);
//		stmt.setString(3, trade.getCodigo());
//		stmt.setFloat(4, trade.getQuantidade());
//		stmt.setDouble(5, Util.converterDinheiroEmDouble(trade.getPreco()));
//		stmt.setDate(6, java.sql.Date.valueOf(trade.getData()));
//		stmt.setString(7, trade.getCompraVenda());
//		stmt.execute();
//		stmt.close();
//	
//		conexao.close();	
	}
	
	
	public void excluirCarteira(Long idUsuario, int numeroCarteira) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;
		
		querySql = "DELETE FROM public.carteira "+
				   " WHERE id_usuario = ? and numero_carteira = ? ";
		
		PreparedStatement stmt = conexao.prepareStatement(querySql);
		
		stmt.setLong(1, idUsuario);
		stmt.setInt(2, numeroCarteira);
		stmt.execute();
		stmt.close();
		conexao.close();
	}
	
	public ArrayList<Trade> buscarCarteira(long idUsuario, int numeroCarteira) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT ativo,quantidade,preco,data,tipo_orden ");
		queryString.append("FROM  public.carteira ");
		queryString.append("WHERE id_usuario = :idUsuario and numero_carteira = :numeroCarteira ");
		Query query = getEntityManager().createNativeQuery(queryString.toString());
		query.setParameter("numeroCarteira", numeroCarteira);
		query.setParameter("idUsuario", idUsuario);
		return  null;
	}
	
//	public ArrayList<Trade> buscarCarteira(long idUsuario, int numeroCarteira) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//		ArrayList<Trade> listaDeTrade = new ArrayList<>();
//		int numeroTrade = 1;
//		Connection conexao = FabricaDeConexao.getConnection();
//		String querySql;
//
//		querySql = "SELECT ativo,quantidade,preco,data,tipo_orden "
//				 + "FROM  public.carteira "
//				 + "WHERE id_usuario = ? and numero_carteira = ? ";
//
//		PreparedStatement stmt = conexao.prepareStatement(querySql);
//
//		stmt.setLong(1, idUsuario);
//		stmt.setInt(2, numeroCarteira);
//	
//		ResultSet rs = stmt.executeQuery();
//
//		while (rs.next()) {
//			numeroTrade++;
//			String data = rs.getString("data");
//			float preco = rs.getFloat("preco");
//			String ativo = rs.getString("ativo");
//			int quantidade = rs.getInt("quantidade");
//			String tipoOrden = rs.getString("tipo_orden");
//			
//			String valorConvertido = Converter.converterDoubleParaDinheiro(preco);
//			double valorTotalOperacao = quantidade * preco;
//			String valorTotalOperacaoConvetido = Converter.converterDoubleParaDinheiro(valorTotalOperacao);
//			
//			Trade trade = new Trade(data, tipoOrden, ativo, quantidade, valorConvertido, numeroTrade, valorTotalOperacaoConvetido);
//			listaDeTrade.add(trade);
//		}
//
//		
//		rs.close();
//		stmt.close();
//		conexao.close();
//		
//		return listaDeTrade;
//	}

}
