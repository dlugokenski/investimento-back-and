package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.enterprise.context.Dependent;

import entity.Arquivo;
import entity.Identificacao;
import entity.Pessoa;
import entity.Usuario;

@Dependent
public class UsuarioDAO extends AbstractPersistence<Pessoa, Long> implements DefaultService<Pessoa>{

	public UsuarioDAO() {
		super(Pessoa.class);
	}

	public void cadastroNovoUsuario(Usuario usuario, String senha)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		try {
			cadastroUsuario(usuario);
			Usuario usuarioCadastrado = pegarUsuarioPeloEmail(usuario.getIdentificacao().getEmail());
			cadastroAutenticacao(usuarioCadastrado, senha);
		} catch (Exception e) {
		}
	}

	public void cadastroUsuario(Usuario usuario)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "INSERT INTO public.usuario(email,nome,sobrenome,cpf)	values(?,?,?,?) ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, usuario.getIdentificacao().getEmail());
		stmt.setString(2, usuario.getNome());
		stmt.setString(3, usuario.getSobrenome());
		stmt.setString(4, usuario.getCpf());
		stmt.execute();
		stmt.close();

		conexao.close();
	}
	
	public void cadastrarAvatarUsuario(Arquivo file) {		
		getEntityManager().getTransaction().begin();
		getEntityManager().merge(file);
		getEntityManager().getTransaction().commit();

	}

	public void cadastroAutenticacao(Usuario usuario, String senha)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "INSERT INTO public.autenticacao(id_usuario,senha,cadastroValidado,contaAtiva,senhaPropria)	values(?,?,?,?,?) ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setLong(1, (long) usuario.getId());
		stmt.setString(2, senha);
		stmt.setBoolean(3, false);
		stmt.setBoolean(4, true);
		stmt.setBoolean(5, true);
		stmt.execute();
		stmt.close();

		conexao.close();
	}
	
	public boolean verificarEmailCadastrado(String email) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		boolean resultado = false;
		
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "SELECT id_usuario "
				 + "FROM  public.usuario "
				 + "WHERE email = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, email);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			resultado = true;
		}

		rs.close();
		stmt.close();
		conexao.close();
		
		return resultado;
	}
		
	public byte[] consultarAvatarUsuario(Long idUsuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		byte[] avatar = getEntityManager().find(Arquivo.class, idUsuario).getFile();
		return avatar;     
	}
	
	public boolean verificarCpfCadastrado(String cpf) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		boolean resultado = false;
		
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "SELECT id_usuario "
				 + "FROM  public.usuario "
				 + "WHERE cpf = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, cpf);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			resultado = true;
		}

		rs.close();
		stmt.close();
		conexao.close();
		
		return resultado;
	}
	
	public Usuario pegarUsuarioPeloEmail(String email) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Usuario usuario = new Usuario();
		
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "SELECT id_usuario,email,nome,sobrenome,cpf "
				 + "FROM  public.usuario "
				 + "WHERE email = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, email);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			usuario.setCpf(rs.getString("cpf"));
			usuario.setId(rs.getInt("id_usuario"));
			usuario.setNome(rs.getString("nome"));
			usuario.setSobrenome(rs.getString("sobrenome"));
			
		}

		rs.close();
		stmt.close();
		conexao.close();
		
		return usuario;
	}
	
	public Usuario pegarUsuarioPeloId(long id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Usuario usuario = new Usuario();
		
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "SELECT email,nome,sobrenome,cpf "
				 + "FROM  public.usuario "
				 + "WHERE id_usuario = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setLong(1, id);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			usuario.setCpf(rs.getString("cpf"));
			usuario.setIdentificacao(new Identificacao(rs.getString("email")));
			usuario.setNome(rs.getString("nome"));
			usuario.setSobrenome(rs.getString("sobrenome"));
			
		}

		rs.close();
		stmt.close();
		conexao.close();
		
		return usuario;
	}
	
	public long pegarIdUsuario(String email) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Long id = null;
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "SELECT id_usuario "
				 + "FROM  public.usuario "
				 + "WHERE email = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, email);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			id = rs.getLong("id_usuario");		
		}

		rs.close();
		stmt.close();
		conexao.close();
		
		return id;
	}
}
