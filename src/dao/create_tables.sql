DROP TABLE IF EXISTS autenticacao;
CREATE TABLE autenticacao(
	id_usuario integer,
	senha VARCHAR(50),
	cadastro_validado boolean,
	conta_ativa boolean,
	senha_propria boolean
)

DROP TABLE IF EXISTS usuario;
CREATE TABLE usuario(
	id_usuario SERIAL PRIMARY KEY,
	email VARCHAR(50),
	nome VARCHAR(50),
	sobrenome VARCHAR(50),
	cpf VARCHAR(50),
	avatar BYTEA
)

DROP TABLE IF EXISTS dadosInstituicao;
CREATE TABLE dadosInstituicao(
	eMail VARCHAR(50),
	senhaEmail VARCHAR(50)
)

DROP TABLE IF EXISTS cotacao;
CREATE TABLE cotacao(
	id_cotacao SERIAL PRIMARY KEY,
	preco REAL,
	ativo VARCHAR(50),
	ultima_atualizacao VARCHAR(50)
)


DROP TABLE IF EXISTS carteira;
CREATE TABLE carteira(
	id_carteira SERIAL PRIMARY KEY,
	id_usuario integer,
	tipo_orden VARCHAR(10),
	numero_carteira INT,
	ativo VARCHAR(10),
	quantidade INT,
	preco REAL,
	data DATE,
	FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario)
)