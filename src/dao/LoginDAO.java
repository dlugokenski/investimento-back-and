package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.enterprise.context.Dependent;
import javax.persistence.Query;

import entity.Login;

@Dependent
public class LoginDAO extends AbstractPersistence<Login, Long> implements DefaultService<Login>{

	public LoginDAO() {
		super(Login.class);
	}
	
	public Login autenticacao(String userName, String senha) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		Login login = new Login();
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("SELECT b.id_usuario, a.senha, a.cadastro_validado, a.conta_ativa, a.senha_propria ");
			queryString.append("FROM  autenticacao AS a ");
			queryString.append("INNER JOIN usuario AS b ON a.id_usuario = b.id_usuario ");
			queryString.append("WHERE b.email = :email and a.senha = :senha ");
			Query query = getEntityManager().createNativeQuery(queryString.toString(), Login.class);
			query.setParameter("email", userName);
			query.setParameter("senha", senha);
			login = (Login) query.getSingleResult();	
		} catch (Exception e) {
			
		}
		return login;
	}
	
	public void validacaoEmail(long idUsuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;
		
		querySql = "UPDATE public.autenticacao "+
				   "SET cadastroValidado = ? "+
				   "WHERE id_usuario = ? ";
		
		PreparedStatement stmt = conexao.prepareStatement(querySql);
	
		stmt.setBoolean(1, true);
		stmt.setLong(2, idUsuario);
		
		stmt.execute();
		stmt.close();
		conexao.close();
	}
}
