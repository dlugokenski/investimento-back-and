package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConsultasInstituicaoDAO {
	
	public static String pegarEmailInstituicao() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		String email = "";
		
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;
		
		querySql = "SELECT eMail "+
				   "FROM  public.dadosInstituicao ";
		
		PreparedStatement stmt = conexao.prepareStatement(querySql);
		
		ResultSet rs2 = stmt.executeQuery();
		
		while (rs2.next()) {
			email = rs2.getString("eMail");
		}
		rs2.close();
		stmt.close();
		conexao.close();
		
		return email;
		
	}
	
	public static String pegarSenhaEmailInstituicao() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{

		
		String senha = null;
		
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;
		
		querySql = "SELECT senhaEmail "+
				   "FROM  public.dadosInstituicao ";
		
		PreparedStatement stmt = conexao.prepareStatement(querySql);
		
		ResultSet rs2 = stmt.executeQuery();
		
		while (rs2.next()) {
			senha = rs2.getString("senhaEmail");
		}
		rs2.close();
		stmt.close();
		conexao.close();
		
		return senha;
		
	}
}
