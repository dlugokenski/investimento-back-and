package rest;

import java.net.URISyntaxException;
import java.sql.SQLException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import business.UsuarioBusiness;
import business.LoginBusiness;

@RequestScoped
@Path("/rest/login/cadastro")
public class CadastroRest extends ResponseData{

	@Inject
    private LoginBusiness loginBusiness;
	
	@Inject
	private UsuarioBusiness usuarioBusiness;
	
	@POST
	@Path("/usuario")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(String Object) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		JSONObject jsonObject = new JSONObject(Object);
		return super.enviarResposta(usuarioBusiness.cadastrarNovoUsuario(jsonObject));
	}
	
	@GET
	@Path("/validacao/{codigo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response validarEmail(@PathParam("codigo") long idUsuario) throws URISyntaxException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		loginBusiness.confirmacaoEmail(idUsuario);
		java.net.URI location = new java.net.URI("../EscritorioModelo");
		return Response.temporaryRedirect(location).build();	
	}
	
}
