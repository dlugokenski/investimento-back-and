package rest;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import business.DefaultBusiness;
import util.Filter;
import util.StringUtil;

public abstract class DefaultRestService<E> extends ResponseData {

	abstract DefaultBusiness<E> getBusiness() throws Exception;
	
	@GET
	@POST
	@Path("/a")
	@Consumes(MediaType.APPLICATION_JSON)
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response ativos() throws Exception {
//		List<Object> ativos = (List) getBusiness().getAtivos();
//		ListaRest<Object> resultado = new ListaRest<Object>();
//		resultado.setList(ativos);
//		resultado.setLength(ativos.size());
//		return ok(resultado);.
		return enviarResposta(null);
	}
	

	@GET
	@POST
	@Path("/id/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByID(@PathParam("id") String id) {
//		try {
//			Long identificador = toLong(id);
//			Object dto = getBusiness().pesquisarByID(identificador);
//			return enviarResposta(dto);
//		} catch (Exception e) {
//			return error(e);
//		}
		return enviarResposta(null);
	}

	@POST
	@Path("/p")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Response pesquisar(Object params) {
		try {
			Map mapa = (Map) params;
			String direction = null;
			if (mapa.get("direction") != null)
				direction = mapa.get("direction").toString();
			String sort = null;
			if (mapa.get("sort") != null)
				sort = mapa.get("sort").toString();
			Integer pageIndex = null;
			if (mapa.get("pageIndex") != null)
				pageIndex = Integer.parseInt(mapa.get("pageIndex").toString());
			Integer pageSize = null;
			if (mapa.get("pageSize") != null)
				pageSize = Integer.parseInt(mapa.get("pageSize").toString());
			mapa.remove("direction");
			mapa.remove("sort");
			mapa.remove("pageIndex");
			mapa.remove("pageSize");

			Filter filter = new Filter();
			filter.setAmountRecords(pageSize);
			filter.setFirstRecord(pageSize * pageIndex);
			if (direction.equals("desc")) {
				filter.setAscendant(false);
			} else {
				filter.setAscendant(true);
			}
			if (StringUtil.isNotEmpty(sort)) {
				filter.setPropertyOrder(sort);
			}

			List<Object> lisaReturn;

			lisaReturn = getBusiness().pesquisar(mapa, filter);
			Integer length = getBusiness().pesquisarCount(mapa);

			return enviarResposta(null);

		} catch (Exception e) {
			return null;
		}
		
	}

}
