package rest;

import java.io.IOException;
import java.sql.SQLException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import business.DefaultBusiness;
import business.UsuarioBusiness;
import entity.Arquivo;
import entity.Pessoa;
import entity.Trade;

@RequestScoped
@Path("/rest/usuario")
public class UsuarioRest extends DefaultRestService<Pessoa> {

	@Inject
    private UsuarioBusiness usuarioBusiness;
	
	@POST
	@Path("/cadastro/avatar")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upLoadSingleFile(@MultipartForm Arquivo file)
			throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		return super.enviarResposta(usuarioBusiness.cadastrarAvatarUsuario(file));
	}
	
	@GET
	@Path("/consulta/dadosUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@QueryParam("idUsuario") @DefaultValue("") long idUsuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		return super.enviarResposta(usuarioBusiness.pegarUsuarioPeloId(idUsuario));
	}
	
	@GET
	@Path("/consulta/avatar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarAvatar(@QueryParam("idUsuario") @DefaultValue("") long idUsuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		return super.enviarResposta(usuarioBusiness.buscarAvatarUsuario(idUsuario));
	}
	

	@Override
	DefaultBusiness<Pessoa> getBusiness() throws Exception {
		return usuarioBusiness;
	}
}
