package rest;

import java.io.IOException;
import java.sql.SQLException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import business.CarteiraBusiness;
import business.DefaultBusiness;
import entity.Arquivo;
import entity.Trade;

@RequestScoped
@Path("/rest/carteira")
public class CarteiraRest extends DefaultRestService<Trade> {

	@Inject
    private CarteiraBusiness carteiraBusiness;
	
	@POST
	@Path("/cadastrar/dadosPlanilha")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(String Object) throws SQLException, IOException{
		return super.enviarResposta(carteiraBusiness.cadastrarCarteira(Object));
	}
	
	@GET
	@Path("/consultar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarAvatar(@QueryParam("idUsuario") @DefaultValue("") long idUsuario, @QueryParam("numeroCarteira") @DefaultValue("0") int numeroCarteira){
		return super.enviarResposta(carteiraBusiness.buscarCarteiraAtual(idUsuario, numeroCarteira));
	}

	@POST
	@Path("/processarDadosExcel")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response processarDadosDeTradeFormatoExcel(@MultipartForm Arquivo file){
		return enviarResposta(carteiraBusiness.processarDadosDeTradeFormatoExcel(file));
	}
	
	@Override
	DefaultBusiness<Trade> getBusiness() throws Exception {
		return carteiraBusiness;
	}
	
}
