package rest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Base64;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.json.JSONObject;

import business.PlanilhaBusiness;
import entity.Arquivo;

@Path("/rest/util")
public class ResponseData {
	
	@Produces(MediaType.APPLICATION_JSON)
	public Response enviarResposta(JSONObject jsonObject) {
		return Response.status(200).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209600").entity(jsonObject.toString()).build();
	}
	
	@POST
	@Path("/converToBase64")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response converterEmBase64(@MultipartForm Arquivo file)
			throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		JSONObject jsonObjectReturn = new JSONObject();
		try{
			jsonObjectReturn.put("result", "success");	
			jsonObjectReturn.put("file", Base64.getEncoder().encodeToString(file.getFile()));
		}catch (Exception e) {
			jsonObjectReturn.put("result", "error");	
		}
		return enviarResposta(jsonObjectReturn);
	}
	
}
