package rest;

import java.sql.SQLException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import business.DefaultBusiness;
import business.LoginBusiness;
import entity.Login;

@RequestScoped
@Path("/rest/login/consulta")
public class LoginRest extends DefaultRestService<Login> {

	@Inject
    private LoginBusiness loginBusiness;

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(String Object) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		JSONObject jsonObject = new JSONObject(Object);
		return super.enviarResposta(loginBusiness.validarDadosLogin(jsonObject));
	}

	@Override
	DefaultBusiness<Login> getBusiness() {
		return loginBusiness;
	}
	
}
