package util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import entity.Arquivo;

public class Converter {

	public static String converterDoubleParaDinheiro(double valor) {
		Locale ptBr = new Locale("pt", "BR");
		String valorString = NumberFormat.getCurrencyInstance(ptBr).format(valor);
		return valorString;
	}
	
	public static InputStream converterByteEmImputStream(Arquivo file) throws IOException {
		InputStream input = new ByteArrayInputStream(file.getFile());
		return input;
	}
	
	public static double arendodarNumeroReal(float numero){
		  return Math.round(numero * 100.0)/100.0;
	}
	
	public static double converterDinheiroEmDouble(String valor) {
		String newString = null;
		newString = valor.toString().replace("R$", "");
		newString = newString.toString().replace(",00", "");
		newString = newString.toString().replace(" ", "");
		newString = newString.toString().replace(".", "");
		newString = newString.toString().replace(",", ".");
	    double resultado = Double.parseDouble(newString);
	    return resultado;
	}
}
