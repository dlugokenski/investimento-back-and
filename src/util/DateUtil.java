package util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;


public class DateUtil {
    
	public static LocalDate DateToLocalDate(Date date) {
		LocalDate localDate = date.toInstant().atZone( ZoneId.systemDefault() ).toLocalDate();
		return localDate;
	}
	
	public static String formatLocalDate (LocalDate date, String formt) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formt);
		return date.format(formatter);
	}
}

