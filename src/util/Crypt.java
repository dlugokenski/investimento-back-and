package util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class Crypt {

    public static String crypt(String str){
        str = stringHexa(generateHash(str, "SHA-256")); 
        return str;
    }
    
    public static byte[] getKey() {
        byte[] key = null;
        try {
            KeyGenerator keygen = KeyGenerator.getInstance("AES");
            keygen.init(128);
            key = keygen.generateKey().getEncoded();
        } catch (NoSuchAlgorithmException ex) {
        	ex.printStackTrace();
        }  
        return key;
    }
    
    public static byte[] getIV() {
       byte[] iv = null;
        try {
            iv = new byte[16];
            SecureRandom random = new SecureRandom();
            random.nextBytes(iv);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }  
        return iv;
    }
    
    public static byte[] encrypt(String value, byte[] key, byte[] iv) {
        byte[] result = null;
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
            byte[] encrypted = cipher.doFinal(value.getBytes());
            result = encrypted;
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchAlgorithmException ex) {
        	result = null;
        }
        return result;
    }
    
    public static String decrypt(byte[] value, byte[] key, byte[] iv) {
        String result = ""; 
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
            byte[] decrypted = cipher.doFinal(value);
            result = new String(decrypted);
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchAlgorithmException ex) {
        	result = null;
        }
        return result;
    }
    
    public static String getHash(byte[] bytes) {
    	String result = "";
    	byte[] hash = null;
    	try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(bytes, 0, bytes.length);
			hash = md.digest();
		} catch (NoSuchAlgorithmException e) {
			hash = null;
			e.printStackTrace();
		}
    	if (hash != null) {
    		StringBuffer hexString = new StringBuffer();
    		for (int i=0;i<hash.length;i++) {
    			hexString.append(0xFF & hash[i]);
    		}
    		result = hexString.toString();
    	}
    	
    	return result;
    }
    
   
    
    private static String stringHexa(byte[] bytes) {
        String s = new String();
        for (int i = 0; i < bytes.length; i++)
        s += Integer.toHexString((((bytes[i] >> 4) & 0xf) << 4) | (bytes[i] & 0xf));
        return s;
    }
  
    private static byte[] generateHash(String phrase, String algorithm) {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            md.update(phrase.getBytes());
            return md.digest();
        } catch (NoSuchAlgorithmException e){return null;} 
    }
    
    private static String normalize1(int i) {
        String result = "";
        if (i < 0) {
        	i = i * -1;
        }
        String i_str = Integer.toString(i);
        if (i_str.length() == 1) {
        	result = result + i;
        } else {
        	result = result + i_str.substring(0,1);
        }
    	return result;
    }
    
    public static String createKey() {
    	String result = "";
    	Random ger = new Random();
    	for (int i=0;i<10;i++) {
    		int n1 = ger.nextInt();
    		result = result + normalize1(n1);
    	}
    	return result;
    }
    
}

