package business;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.inject.Inject;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import dao.CarteiraDAO;
import entity.AporteMensal;
import entity.Arquivo;
import entity.Ativo;
import entity.Trade;
import util.Converter;
import util.DateUtil;
import util.Filter;
import util.StringUtil;

@Stateful
public class CarteiraBusiness extends DefaultBusiness<Trade> implements Serializable {
	
	@Inject
	CarteiraBusiness carteiraBusiness;
	
	@Inject
	private CarteiraDAO carteiraDAO;
	
	@Inject
	private PlanilhaBusiness panilhaBusiness;
	
	public JSONObject cadastrarCarteira(String Object) throws SQLException, IOException{ 
		
		JSONObject jsonObject = new JSONObject(Object);
		JSONObject jsonObjectReturn = new JSONObject();
		Gson gson = new Gson();
		JSONArray jsonArray = new JSONArray();
		
		try{
			Long idUsuario = Long.parseLong(jsonObject.getString("idUsuario"));
			carteiraDAO.excluirCarteira(idUsuario,0);
			jsonArray = jsonObject.getJSONArray("data");
			for (int i = 0; i < jsonArray.length(); i++){
				JSONObject jsonObject1 = jsonArray.getJSONObject(i);
				Trade trade = gson.fromJson(jsonObject1.toString(), Trade.class);
				carteiraDAO.cadastrarCadastroCarteira(trade, idUsuario, 0);
			}
			jsonObjectReturn.put("result", "success");
			jsonObjectReturn.put("mensagen", "Dados salvo com sucesso!");
			return jsonObjectReturn;
		} catch (Exception e) {
			return StringUtil.retornarErro("Ocorreu um erro ao salvar os dados");
		}
	}
	
	public JSONObject buscarCarteiraAtual(long idUsuario, int numeroCarteira){	
		try{
			return percorrerMontarCarteiraAtual(carteiraDAO.buscarCarteira(idUsuario, numeroCarteira));
		} catch (Exception e) {
			return StringUtil.retornarErro("Ocorreu um erro recuperar os dados da carteira!");
		}
	}
	
	public Trade calcularOperacaoTrade(Trade tradeOne, Trade tradeTwo, String operacao){
		float quantidadeRestante = 0;
		float dinheiroColocado = 0;
		
		if(operacao.equals("somar")){
			quantidadeRestante= tradeOne.getQuantidade() + tradeTwo.getQuantidade();
			dinheiroColocado = tradeOne.getValorTotalOperacao() + tradeTwo.getValorTotalOperacao();
		}else{
			quantidadeRestante= tradeOne.getQuantidade() - tradeTwo.getQuantidade();
			dinheiroColocado = tradeOne.getValorTotalOperacao() - tradeTwo.getValorTotalOperacao();
		}
	
		int newNumeroTrade = tradeTwo.getNumeroDoTrade();

		Trade newTrade = new Trade( tradeTwo.getData(), tradeTwo.getCompraVenda(), tradeTwo.getCodigo(), quantidadeRestante, tradeTwo.getPreco(), newNumeroTrade, dinheiroColocado);
		return newTrade;
	}
	
	public JSONObject percorrerMontarCarteiraAtual(ArrayList<Trade> listaDeTrades){
		JSONObject jsonObject = new JSONObject();
		List<Trade> carteiraAtual = new ArrayList<Trade>();
		
		for(int i = 0; i < listaDeTrades.size(); i++){	
			Trade tradeVez = listaDeTrades.get(i);
			if (tradeVez.getCompraVenda().equals("C")) {
				boolean ativoEncontrado = false;
				for (int d = 0; d < carteiraAtual.size(); d++) {
					if (carteiraAtual.get(d).getCodigo().equals(tradeVez.getCodigo())) {
						Trade trade = calcularOperacaoTrade(carteiraAtual.get(d), tradeVez, "somar");
						ativoEncontrado = true;						
						carteiraAtual.remove(d);
						carteiraAtual.add(d, trade);
						break;
					}
				}
				if (!ativoEncontrado) {
					Trade trade = listaDeTrades.get(i);
					carteiraAtual.add(trade);
				}

			} else {
				for (int c = 0; c < carteiraAtual.size(); c++) {
					if (carteiraAtual.get(c).getCodigo().equals(listaDeTrades.get(i).getCodigo())) {
						Trade trade = calcularOperacaoTrade(carteiraAtual.get(c), tradeVez, "subtrair");
						carteiraAtual.remove(c);
						if(trade.getQuantidade() > 0){
							carteiraAtual.add(trade);
						} 
						break;
					}
				}
			}
		}
		
		jsonObject.put("result", "success");
		jsonObject.put("mensagen", "Dados salvo com sucesso!");
		jsonObject.put("carteiraAtual", carteiraAtual);
		return jsonObject;
	}
	
	public JSONObject montarAporteMensal(ArrayList<Trade> listaDeTrade) throws ParseException{
		ArrayList<AporteMensal> listaRetorno = new ArrayList<>();		
		
		for(int i = 0; i < listaDeTrade.size(); i++){
			int ano = Integer.parseInt(DateUtil.formatLocalDate(listaDeTrade.get(i).getData(), "YYYY"));
			int mes = Integer.parseInt(DateUtil.formatLocalDate(listaDeTrade.get(i).getData(), "MM"));
			String verificarExisteElementoLista = verificarSeListaContemAnoMes(listaRetorno, ano, mes);
			if(verificarExisteElementoLista != "nao encontrado"){
				int indexListaAporteRetorno = Integer.parseInt(verificarExisteElementoLista);
				List<Trade> list = listaRetorno.get(indexListaAporteRetorno).getListaDeTrade();
				list.add(listaDeTrade.get(i));
				listaRetorno.get(indexListaAporteRetorno).setListaDeTrade(list);
			} else {
				List<Trade> list = new ArrayList<Trade>();
				list.add(listaDeTrade.get(i));
				AporteMensal newAporte = new AporteMensal(mes, ano, list);
				listaRetorno.add(newAporte);
			}
		}
	
		return calcularAporteTotalMensal(listaRetorno);
		
	}
	
	public JSONObject calcularAporteTotalMensal(ArrayList<AporteMensal> lista){
		JSONObject jsonObject = new JSONObject();
		
		for(int i = 0; i < lista.size(); i++){
			float somaDeSaldo = 0;
			for(int a = 0; a < lista.get(i).getListaDeTrade().size(); a++){
				Trade tradeVez = lista.get(i).getListaDeTrade().get(a);
				if(tradeVez.getCompraVenda().equals("C")){
					somaDeSaldo += tradeVez.getValorTotalOperacao();
				} else {
					somaDeSaldo -= tradeVez.getValorTotalOperacao();
				}
			}
			lista.get(i).setValorTotal(somaDeSaldo);
		}
		jsonObject.put("aportesMensal", lista);
		return jsonObject;
	}
		
	public String verificarSeListaContemAnoMes(ArrayList<AporteMensal> lista, int ano, int mes){
		for(int i =0; i < lista.size(); i++){
			if(lista.get(i).getAnoCopetencia() == ano && lista.get(i).getMesCopetencia() == mes){
				return String.valueOf(i);
			}
		}
		return "nao encontrado";
	}

	public JSONObject montarDadosCarteira(List<Trade> listaTrabalhada) {
		JSONObject jsonObjectReturn = new JSONObject();
		ArrayList<Trade> listaDeTrade = new ArrayList<Trade>();
		ArrayList<Trade> resultadoFinanceiro = new ArrayList<Trade>();
		ArrayList<Trade> carteiraAtual = new ArrayList<Trade>();
		
		try {
			
			for(int i = 0; i < listaTrabalhada.size(); i++) {
				
				Trade tradeVez = listaTrabalhada.get(i);
				if (tradeVez.getCompraVenda().equals("C")) {

					boolean ativoEncontrado = false;
					for (int d = 0; d < carteiraAtual.size(); d++) {
						if (carteiraAtual.get(d).getCodigo().equals(tradeVez.getCodigo())) {
							ativoEncontrado = true;
							float quantidadeRestante = carteiraAtual.get(d).getQuantidade() + tradeVez.getQuantidade();
							quantidadeRestante = (float) Converter.arendodarNumeroReal(quantidadeRestante);
							int newNumeroTrade = carteiraAtual.get(d).getNumeroDoTrade();
							String tipo = carteiraAtual.get(d).getTipo();
							Trade newTrade = new Trade(tradeVez.getData(), tradeVez.getCompraVenda(), tradeVez.getCodigo(), quantidadeRestante, tradeVez.getPreco(), newNumeroTrade, tradeVez.getPreco(), tipo);
							carteiraAtual.remove(d);
							carteiraAtual.add(d, newTrade);
							break;
						}
					}
					if (!ativoEncontrado) {
						carteiraAtual.add(tradeVez);
					}

				} else if (!verificarCalculoDeVendaTrade(tradeVez, carteiraAtual)) {
					jsonObjectReturn.put("result", "error");
					jsonObjectReturn.put("mensagen", "Não coincide a quantidade de " + tradeVez.getQuantidade() + " do ativo "
							+ tradeVez.getCodigo() + " na sua carteira no perido anterior á " + DateUtil.formatLocalDate(tradeVez.getData(), "dd/MM/yyyy"));
					return jsonObjectReturn;
				} else {
					for (int c = 0; c < carteiraAtual.size(); c++) {
						if (carteiraAtual.get(c).getCodigo().equals(tradeVez.getCodigo())) {
							float quantidadeRestante = carteiraAtual.get(c).getQuantidade() - tradeVez.getQuantidade();
							quantidadeRestante = (float) Converter.arendodarNumeroReal(quantidadeRestante);
							int newNumeroTrade = carteiraAtual.get(c).getNumeroDoTrade();
							float resultoFinanceiroDoTrade = carteiraAtual.get(c).getValorTotalOperacao() - tradeVez.getValorTotalOperacao();
							Trade tradeResultadoFinanceiro = calcularRentabilidadeTrade(tradeVez, carteiraAtual.get(c));
							String tipo = carteiraAtual.get(c).getTipo();
							resultadoFinanceiro.add(tradeResultadoFinanceiro);
							Trade newTrade = new Trade(tradeVez.getData(), tradeVez.getCompraVenda(), tradeVez.getCodigo(), quantidadeRestante, tradeVez.getPreco(), newNumeroTrade, resultoFinanceiroDoTrade, tipo);
							carteiraAtual.remove(c);
							carteiraAtual.add(c, newTrade);
							if (carteiraAtual.get(c).getQuantidade() == 0) {
								carteiraAtual.remove(c);
							}
							break;
						}
					}
				}
			}
		

			//Collections.reverse(listaDeTrade);
			
			jsonObjectReturn.put("aportesMensal", carteiraBusiness.montarAporteMensal(listaDeTrade));
			jsonObjectReturn.put("ultimaAtualizacaoCotacao", ultimaAtualizacaoCotacao());
			jsonObjectReturn.put("resultadoFinanceiro", resultadoFinanceiro);
			jsonObjectReturn.put("ativosValorAtual", listaDeValoesDosAtivosAtuais(carteiraAtual));
			jsonObjectReturn.put("result", "success");
			jsonObjectReturn.put("carteiraAtual", carteiraAtual);
			jsonObjectReturn.put("data", listaDeTrade);
			return jsonObjectReturn;
		} catch (Exception e) {
			System.out.println(e);
			return StringUtil.retornarErro("Ocorreu um erro ao ler a planilha!");
		}
	}
	
	public JSONObject processarDadosDeTradeFormatoExcel(Arquivo file) {
		try {
			List<Trade> listaTrabalhada = panilhaBusiness.lerPlanilha(file);
			return montarDadosCarteira(listaTrabalhada);
		} catch (Exception e) {
			return StringUtil.retornarErro("Ocorreu um erro ao ler a planilha!");
		}
	}


	public Trade calcularRentabilidadeTrade(Trade tradeSendoRealizadoAgora, Trade tadreCarteiraAtual) {
		float precoMedioTadreCarteiraAtual = tadreCarteiraAtual.getValorTotalOperacao() / tadreCarteiraAtual.getQuantidade();
		float resultadoFinanceiro = tradeSendoRealizadoAgora.getValorTotalOperacao() - (precoMedioTadreCarteiraAtual * tradeSendoRealizadoAgora.getQuantidade());
		return new Trade(tradeSendoRealizadoAgora.getCodigo(), resultadoFinanceiro);
	}

	public String ultimaAtualizacaoCotacao() throws InstantiationException, IllegalAccessException,ClassNotFoundException, SQLException, ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat formatoApresentacao = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date data = null;
		String dataBanco = carteiraDAO.pergarUltimaDataAtualizacao();
		data = formato.parse(dataBanco);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.HOUR, 1);
		return formatoApresentacao.format(calendar.getTime());
	}

	public boolean verificarCalculoDeVendaTrade(Trade trade, ArrayList<Trade> carteiraAtual) {
		for (int i = 0; i < carteiraAtual.size(); i++) {
			if (carteiraAtual.get(i).getCodigo() == trade.getCodigo()
					&& carteiraAtual.get(i).getQuantidade() >= trade.getQuantidade()) {
				return true;
			}
		}
		return false;
	}

	public ArrayList<Ativo> listaDeValoesDosAtivosAtuais(ArrayList<Trade> carteiraAtual) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		ArrayList<Ativo> listaDeAtivos = new ArrayList<Ativo>();
		for (int i = 0; i < carteiraAtual.size(); i++) {
			if(carteiraAtual.get(i).getTipo().equals("acao")){
				if(carteiraDAO.verificarSeAcaoExiste(carteiraAtual.get(i).getCodigo())){
					float valorAtualAtivo = carteiraDAO.pegarCotacaoAtualAcao(carteiraAtual.get(i).getCodigo());
					Ativo ativo = new Ativo(carteiraAtual.get(i).getCodigo(), valorAtualAtivo);
					listaDeAtivos.add(ativo);
				}
			
			} else if(carteiraAtual.get(i).getTipo().equals("tesouro")) {
				if(carteiraDAO.verificarSeTituloPublicoExiste(carteiraAtual.get(i).getCodigo())){
					float valorAtualAtivo = carteiraDAO.pegarCotacaoAtualTesouro(carteiraAtual.get(i).getCodigo());
					Ativo ativo = new Ativo(carteiraAtual.get(i).getCodigo(), valorAtualAtivo);
					listaDeAtivos.add(ativo);
				}				
			}			
		}
		return listaDeAtivos;
	}
	@Override
	public Object pesquisarByID(Long identificador) throws Exception {
		return null;
	}

	@Override
	public List<?> pesquisar(Map<String, Object> map, Filter filter) throws Exception {
		return null;
	}

	@Override
	public Integer pesquisarCount(Map<String, Object> map) throws Exception {
		return null;
	}

	@Override
	public List<?> getAtivos() throws Exception {
		return null;
	}
}
