package business;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.json.JSONObject;

import dao.UsuarioDAO;
import dao.CarteiraDAO;
import dao.UsuarioDAO;
import dao.ConsultasInstituicaoDAO;
import entity.Arquivo;
import entity.EnviarEmail;
import entity.Identificacao;
import entity.Pessoa;
import entity.Trade;
import entity.Usuario;
import util.Filter;

@Dependent
public class UsuarioBusiness extends DefaultBusiness<Pessoa> implements Serializable {
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	public JSONObject cadastrarNovoUsuario(JSONObject jsonObject) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		JSONObject jsonObjectReturn = new JSONObject();
		Usuario usuario = converterJsonObjectEmUsuario(jsonObject);
		if(usuarioDAO.verificarEmailCadastrado(usuario.getIdentificacao().getEmail()) == false){
			if(usuarioDAO.verificarCpfCadastrado(usuario.getCpf()) == false){
				usuarioDAO.cadastroNovoUsuario(usuario, pegarAtributoSenhaJson(jsonObject));
				enviarEmailCadastroUsuario(usuario, usuarioDAO.pegarIdUsuario(usuario.getIdentificacao().getEmail()));
			} else {
				jsonObjectReturn.put("error", "CPF já cadastrado");
				return jsonObjectReturn;
			}						
		} else {
			jsonObjectReturn.put("error", "E-mail já cadastrado");
			return jsonObjectReturn;
		}
		jsonObjectReturn.put("success", "Cadastro efetuado com sucesso!");
		return jsonObjectReturn;
	}
		
	public Usuario converterJsonObjectEmUsuario(JSONObject jsonObject){
		String email = jsonObject.getString("email");
		String nome = jsonObject.getString("nome");
		String sobrenome = jsonObject.getString("sobrenome");
		Identificacao identificacao = new Identificacao(email);
		Usuario usuario = new Usuario(identificacao);
		usuario.setNome(nome);
		usuario.setSobrenome(sobrenome);
		return usuario;
	}
	
	public String pegarAtributoSenhaJson(JSONObject jsonObject){
		String senha = jsonObject.getString("senha");
		return senha;
	}
	
	public JSONObject pegarUsuarioPeloId(long id){
		JSONObject jsonObjectReturn = new JSONObject();
		
		try{
			Usuario usuario = usuarioDAO.pegarUsuarioPeloId(id);
			jsonObjectReturn.put("nome", usuario.getNome());
			jsonObjectReturn.put("sobrenome", usuario.getSobrenome());
			jsonObjectReturn.put("email", usuario.getIdentificacao().getEmail());	
		}catch (Exception e) {
			jsonObjectReturn.put("mensagem", "sessao_expirada");
		}
		
		return jsonObjectReturn;
	}
	public void enviarEmailCadastroUsuario(Usuario usuario, Long id_usuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		ConsultasInstituicaoDAO daoConsultasInstituicao = new ConsultasInstituicaoDAO();
		String email = daoConsultasInstituicao.pegarEmailInstituicao();
		String senha = daoConsultasInstituicao.pegarSenhaEmailInstituicao();
		String assunto = "Validação de e-mail";
		
		StringBuilder corpoDoEmail = new StringBuilder();
		
		corpoDoEmail.append("<center><h2>Olá, " +usuario.getNome()+" "+usuario.getSobrenome()+"</h2></center>");
		corpoDoEmail.append("<br/><br/>");
		corpoDoEmail.append("<center>Click no link abaixo para confirmação de cadastro no Minha Rentabilidade</center>");
		corpoDoEmail.append("<br/>");
		corpoDoEmail.append("<br/>");
		corpoDoEmail.append("<center><a href='http://localhost:3000/invertor/rest/login/cadastro/validacao/"+id_usuario+"'>Verificar email</a></center>");
		corpoDoEmail.append("<br/>");
		corpoDoEmail.append("<center>Att: <a href='http://localhost:3000/invertor'>www.minharentabilidade.com.br</a></center>");

	
		EnviarEmail enviarEmail = new EnviarEmail(usuario,email,senha,assunto,corpoDoEmail);
		Thread threadEnviarEmail = new Thread(enviarEmail);
		threadEnviarEmail.start();
	}
	public JSONObject cadastrarAvatarUsuario(Arquivo file) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		JSONObject jsonObjectReturn = new JSONObject();
		try{
			usuarioDAO.cadastrarAvatarUsuario(file);
			jsonObjectReturn.put("result", "success");	
			jsonObjectReturn.put("mensagen", "Imagen alterada com sucesso!");	
		}catch (Exception e) {
			jsonObjectReturn.put("result", "error");	
			jsonObjectReturn.put("mensagen", "Ocorreu um erro ao adicionar a imagen!");	
		}
		return jsonObjectReturn;		
	}
	public JSONObject buscarAvatarUsuario(long idUsuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		JSONObject jsonObjectReturn = new JSONObject();
		if(usuarioDAO.consultarAvatarUsuario(idUsuario) != null){
			byte[] bytes = usuarioDAO.consultarAvatarUsuario(idUsuario);
			jsonObjectReturn.put("file", Base64.getEncoder().encodeToString(bytes));	
		} else {
			jsonObjectReturn.put("error", "usuarioNãoPossuiFoto");	
		}
			
		return jsonObjectReturn;
	}

	@Override
	public Object pesquisarByID(Long identificador) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> pesquisar(Map<String, Object> map, Filter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer pesquisarCount(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getAtivos() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
