package business;

import java.util.List;
import java.util.Map;

import util.Filter;

public abstract class DefaultBusiness<E> {

	public abstract Object pesquisarByID(Long identificador) throws Exception;

	public abstract List<?> pesquisar(Map<String, Object> map, Filter filter) throws Exception;

	public abstract Integer pesquisarCount(Map<String, Object> map) throws Exception;

	public abstract List<?> getAtivos() throws Exception;
	
}
