package business;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.Dependent;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import entity.Arquivo;
import entity.Trade;
import util.Converter;
import util.DateUtil;

@Dependent
public class PlanilhaBusiness {

	private XSSFWorkbook workbook;

	public List<Trade> lerPlanilha(Arquivo file) {
		List<Trade> listaDeTrade = new ArrayList<Trade>();
		try {

			workbook = new XSSFWorkbook(Converter.converterByteEmImputStream(file));

			// recupera apenas a primeira aba da planilhas disponiveis
			XSSFSheet sheet = workbook.getSheetAt(0);
			for (int i = 1; i < sheet.getLastRowNum() + 1; i++) {
				Row linhaDaVez = sheet.getRow(i);
				Date dataDate = linhaDaVez.getCell(0).getDateCellValue();
				String compraVenda = linhaDaVez.getCell(1).getStringCellValue();
				String tesouroAcao = linhaDaVez.getCell(2).getStringCellValue();
				String ativo = linhaDaVez.getCell(3).getStringCellValue();
				float quantidadeDouble = (float) linhaDaVez.getCell(4).getNumericCellValue();
				float preco = (float) linhaDaVez.getCell(5).getNumericCellValue();
				
				tesouroAcao = tesouroAcao.toString().replace("Ç", "C");
				tesouroAcao = tesouroAcao.toString().replace("Ã", "A");
				tesouroAcao = tesouroAcao.toLowerCase();
	
				LocalDate localDate = DateUtil.DateToLocalDate(dataDate);
				float quantidade = (float) quantidadeDouble;
				float valorTotalOperacao = quantidade * preco;

				Trade trade = new Trade(localDate, compraVenda, ativo, quantidade, preco, i, valorTotalOperacao, tesouroAcao);
				listaDeTrade.add(trade);

			}
		} catch (Exception e) {
			listaDeTrade.clear();
		}
		return listaDeTrade;
	}
}
