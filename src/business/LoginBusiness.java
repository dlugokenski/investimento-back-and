package business;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ejb.Stateful;
import javax.inject.Inject;

import org.json.JSONObject;

import dao.LoginDAO;
import entity.Login;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import util.Filter;

@Stateful
public class LoginBusiness extends DefaultBusiness<Login> implements Serializable {

	@Inject
    private LoginDAO loginDAO;

	
	final static String SECRETKEY = "35725c901c45f1c13f9e3fe8421a15dd26130118";

	public String gerarToken(String nome) {
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis + 86400000);

		String token = Jwts.builder().setSubject(nome).claim("auth", "usuario")
				.signWith(SignatureAlgorithm.HS512, SECRETKEY).setExpiration(now).compact();

		return token;
	}
	
	public void confirmacaoEmail(long idUsuario) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{		
		loginDAO.validacaoEmail(idUsuario);
	}
	
	public JSONObject validarDadosLogin(JSONObject jsonObject) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{	
		Login loginClass = new Login();
		String login = jsonObject.getString("login");
		String senha = jsonObject.getString("senha");
		loginClass = loginDAO.autenticacao(login, senha);
		JSONObject jsonObjectReturn = new JSONObject();
		if(loginClass != null){
			jsonObjectReturn.put("autenticacao", "success");
			jsonObjectReturn.put("id_usuario", loginClass.getId());
			jsonObjectReturn.put("contaAtiva", loginClass.isContaAtiva());
			jsonObjectReturn.put("cadastroValidado", loginClass.getCadastroValidado());
			jsonObjectReturn.put("senhaPropria", loginClass.isSenhaPropria());
			jsonObjectReturn.put("tokenHash", gerarToken(login));
		}else {
			jsonObjectReturn.put("autenticacao", "error");
		}	
		return jsonObjectReturn;		
	}
	
	public boolean validateToken(String authToken) {
		boolean resultado = false;
		
		if (authToken.equals("naoEncontrado")) {
			resultado = false;
		}else{
			try {
			Jwts.parser().setSigningKey(SECRETKEY).parseClaimsJws(authToken);
			resultado = true;
		} catch (SignatureException e) {
			resultado = false;
		}
		}
		return resultado;
	}
	
	public JSONObject validarToken(String authToken){
		JSONObject jsonObject = new JSONObject();		
		if(validateToken(authToken) == true){
			jsonObject.put("resutladoAutenticacao", "permitido");
		}else{
			jsonObject.put("resutladoAutenticacao", "negado");
		} 
		return jsonObject;
	}
	
	public String gerarSenhaTemporaria(){
		UUID uuid = UUID.randomUUID();
		String myRandom = uuid.toString();
		myRandom = myRandom.substring(0,6);
		System.out.println(myRandom);
		return myRandom;
	}

	@Override
	public Object pesquisarByID(Long identificador) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> pesquisar(Map<String, Object> map, Filter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer pesquisarCount(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getAtivos() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
